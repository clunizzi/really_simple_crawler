class CrawlerController < ApplicationController
  
  def new
  end

  def create
    crawler = crawler_params
    redirect_to root_url if crawler[:site].empty?
    crawl!(crawler)
  end
  
    private

    #Add http/s prefix if not present
    def setting_url(url)
      unless url.start_with?("http://", "https://")
        url = "http://" + url 
      else
        url
      end
     # url.insert(7, "www.") unless url.include?("www.")
     # url
    end
      
    #Crawling the site
    def crawl!(crawler)
      @links = {}
      url = setting_url(crawler[:site])
      depth = crawler[:deep].to_i
      Anemone.crawl(url, 
                    :discard_page_bodies => true, 
                    :depth_limit=>depth) do |anemone|
                      anemone.on_every_page do |page|
                      @links[page.url] = page.referer
                    end
      end
    end

    #Accept only this params
    def crawler_params
      params.require(:crawler).permit(:site, :deep)
    end
  
end
