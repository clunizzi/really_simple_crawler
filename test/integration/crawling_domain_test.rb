require 'test_helper'

class CrawlingDomainTest < ActionDispatch::IntegrationTest
 
 test "should not allow empty field" do
    get root_path
    assert_template 'crawler/new'
    post crawler_path, crawler: { site: "", 
                                  deep: 2 }
    assert_redirected_to root_url
  end
  
  test "should crawl domain without http || https" do
    get root_path
    post crawler_path, crawler: { site: "www.google.it", 
                                  deep: 2 }
    assert_match "http://www.google.it", response.body
  end

end

